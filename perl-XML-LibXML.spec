Name:           perl-XML-LibXML
Version:        2.0210
Release:        2
Epoch:          1
Summary:        Perl Binding for libxml2
License:        (GPL-1.0-or-later OR Artistic-1.0-Perl) and MIT
URL:            https://metacpan.org/release/XML-LibXML
Source0:        https://cpan.metacpan.org/authors/id/S/SH/SHLOMIF/XML-LibXML-%{version}.tar.gz

BuildRequires:  coreutils, findutils, glibc-common, libxml2-devel
BuildRequires:  perl-interpreter, perl-devel, perl-generators, sed
BuildRequires:  perl(Config), perl(Cwd), perl(Devel::CheckLib), perl(ExtUtils::MakeMaker)
BuildRequires:  perl(File::Spec), perl(lib), perl(strict), perl(Symbol), perl(vars), perl(warnings)
BuildRequires:  perl(base), perl(Carp), perl(constant), perl(Data::Dumper), perl(DynaLoader)
BuildRequires:  perl(Encode), perl(Exporter), perl(IO::File), perl(IO::Handle), perl(overload)
BuildRequires:  perl(parent), perl(Scalar::Util), perl(Tie::Hash), perl(XML::NamespaceSupport)
BuildRequires:  perl(XML::SAX::Base), perl(XML::SAX::DocumentLocator), perl(XML::SAX::Exception)
BuildRequires:  perl(Errno), perl(locale), perl(POSIX), perl(Test::More), perl(XML::SAX)
BuildRequires:  perl(XML::SAX::ParserFactory), perl(URI::file), perl(utf8), perl(Alien::Base::Wrapper)
BuildRequires:  perl(Alien::Libxml2)

Requires: libxml2 >= %(rpm -q --queryformat="%%{VERSION}" libxml2)
Requires(preun):    perl-interpreter
Provides:       perl-XML-LibXML-Common = %{version}
Obsoletes:      perl-XML-LibXML-Common <= 0.13

%description
This module is Perl Binding for libxml2, which implements a Perl interface to
the GNOME libxml2 library. This module provides interfaces for parsing and
manipulating XML files and allows Perl programmers to use the highly capable
validating XML parser and the high performance DOM implementation.

%package_help

%prep
%autosetup -n XML-LibXML-%{version} -p1
chmod -x *.c
for i in Changes; do
  /usr/bin/iconv -f iso8859-1 -t utf-8 $i > $i.conv && /bin/mv -f $i.conv $i
done

sed -i -e '/^inc\// d' MANIFEST

%build
perl Makefile.PL SKIP_SAX_INSTALL=1 INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT NO_PACKLIST=1
chmod -R u+w $RPM_BUILD_ROOT/*

%check
THREAD_TEST=0 make test

%triggerin -- perl-XML-SAX
for p in XML::LibXML::SAX::Parser XML::LibXML::SAX ; do
  /usr/bin/perl -MXML::SAX -e "XML::SAX->add_parser(q($p))->save_parsers()" \
    2>/dev/null || :
done

%preun
if [ $1 -eq 0 ] ; then
  for p in XML::LibXML::SAX::Parser XML::LibXML::SAX ; do
    /usr/bin/perl -MXML::SAX -e "XML::SAX->remove_parser(q($p))->save_parsers()" \
      2>/dev/null || :
  done
fi

%files
%license LICENSE
%doc HACKING.txt README
%{perl_vendorarch}/auto/XML
%{perl_vendorarch}/XML

%files help
%{_mandir}/man*/*

%changelog
* Thu Jan 16 2025 Funda Wang <fundawang@yeah.net> - 1:2.0210-2
- drop useless perl(:MODULE_COMPAT) requirement
- hardcode libxml2 version on runtime

* Wed Jul 17 2024 wuzhaomin <wuzhaomin@kylinos.cn> - 1:2.0210-1
- Upgrade to version 2.0210
- Fix copying external entity from an ext_ent_handler handler
- Fix function prototypes in function pointers

* Thu Aug 10 2023 liyanan <thistleslyn@163.com> - 1:2.0209-1
- Update to 2.0209

* Thu Jan 19 2023 yangbo <yangbo1@xfusion.com> - 1:2.0207-4
- Correct some broken grammar / spelling / syntax / etc

* Wed Jan 18 2023 yangbo <yangbo1@xfusion.com> - 1:2.0207-3
- Add <meta> sniffing warning

* Fri Nov 19 2021 yuanxin <yuanxin24@huawei.com> - 1:2.0207-2
- delete the old tar package

* Thu Nov 18 2021 yuanxin <yuanxin24@huawei.com> - 1:2.0207-1
- bump to version 2.0207

* Wed Jul 22 2020 dingyue <dingyue5@huawei.com> - 1:2.0205-1
- bump to version 2.0205

* Tue Nov 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:2.0132-5
- Package init
